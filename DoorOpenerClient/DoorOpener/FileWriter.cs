﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace DoorOpener
{
    public class FileWriter
    {
        protected string targetPath = null;
        public FileWriter()
        {
            string basePath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string fileName = "settings.txt";
            this.targetPath = Path.Combine(basePath, fileName);
        }

        public void SaveToFile(DoorModel model)
        {
            string content = JsonConvert.SerializeObject(model);
            File.WriteAllText(targetPath, content);
        }

        public DoorModel RestoreFromFile()
        {
            if(!File.Exists(this.targetPath))
            {
                return null;
            }
            try
            {
                string content = File.ReadAllText(this.targetPath);
                DoorModel model = JsonConvert.DeserializeObject<DoorModel>(content);
                return model;
            }
            catch(Exception e)
            {
                //corrupt file (or old version). Delete the old file
                File.Delete(this.targetPath);
            }
            return null;
        }
    }
}
