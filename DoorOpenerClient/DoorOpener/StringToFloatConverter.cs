﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace DoorOpener
{
    public class StringToFloatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return value.ToString();
            }
            catch( Exception e)
            {
                return "";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return float.Parse((String)value);
            }
            catch(Exception e)
            {
                return null;
            }
        }
    }
}
