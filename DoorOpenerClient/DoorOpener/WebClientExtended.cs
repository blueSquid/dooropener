﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace DoorOpener
{
    public class WebClientExtended : WebClient
    {
        private IPAddress sourceIpAddress = null;

        protected override WebRequest GetWebRequest(Uri address)
        {
            if(this.sourceIpAddress == null)
            {
                return base.GetWebRequest(address);
            }

            //force the outbound connect over the wifi network adapter specified by the SourceIpAddress
            WebRequest request = (WebRequest)base.GetWebRequest(address);

            ((HttpWebRequest)request).ServicePoint.BindIPEndPointDelegate += (servicePoint, remoteEndPoint, retryCount) => {
                //setting the port number to 0 notifies the TCP stack to assign the next free one.
                int portNum = 0;
                return new IPEndPoint(this.sourceIpAddress, portNum);
            };

            return request;
        }

        public Task<string> DownloadStringTaskExtendAsync(string urlAddress, IPAddress mSourceAddress = null)
        {
            this.sourceIpAddress = mSourceAddress;
            return this.DownloadStringTaskAsync(urlAddress);
        }
    }
}
