﻿using System;
using System.ComponentModel;

namespace DoorOpener
{
    [Serializable]
    public class DoorModel : INotifyPropertyChanged
    {
        public DoorModel()
        {
            
        }

        public DoorModel(DoorModel model)
        {
            this.triggerUrl = model.triggerUrl;
            this.connectToSsidFlag = model.connectToSsidFlag;
            this.mobileNetworkFlag = model.mobileNetworkFlag;
            this.timeoutValue = model.timeoutValue;
        }

        protected string ssid = null;
        public string Ssid
        {
            get
            {
                return ssid;
            }
            set
            {
                if (ssid == null || !ssid.Equals(value))
                {
                    ssid = value;
                    OnPropertyChanged("Ssid");
                }
            }
        }

        protected string triggerUrl = null;
        public string TriggerUrl
        {
            get
            {
                return triggerUrl;
            }
            set
            {
                if (triggerUrl == null || !triggerUrl.Equals(value))
                {
                    triggerUrl = value;
                    OnPropertyChanged("TriggerUrl");
                }
            }
        }

        protected bool connectToSsidFlag = false;
        public bool ConnectToSsidFlag
        {
            get
            {
                return connectToSsidFlag;
            }
            set
            {
                if (connectToSsidFlag != value)
                {
                    connectToSsidFlag = value;
                    OnPropertyChanged("ConnectToSsidFlag");
                }
            }
        }

        protected bool mobileNetworkFlag = false;
        public bool EnableMobileNetworkFlag
        {
            get
            {
                return mobileNetworkFlag;
            }
            set
            {
                if (mobileNetworkFlag != value)
                {
                    mobileNetworkFlag = value;
                    OnPropertyChanged("EnableMobileNetworkFlag");
                }
            }
        }

        protected float? timeoutValue = 5.0f;
        public string TimeoutValueStr
        {
            get
            {
                return timeoutValue.ToString();
            }
            set
            {
                try
                {
                    if(((string)value).Length == 0)
                    {
                        this.timeoutValue = null;
                    }
                    float timeoutFloat = float.Parse(value);
                    if (!timeoutValue.Equals(timeoutFloat))
                    {
                        timeoutValue = timeoutFloat;
                        OnPropertyChanged("TimeoutValueStr");
                    }
                }
                catch(Exception e)
                {
                    //update view to previous value
                    OnPropertyChanged("TimeoutValueStr");
                }
            }
        }

        public float TimeoutValueFloat
        {
            get
            {
                return this.timeoutValue.Value;
            }
        }


        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null) 
            {
                return;
            }
            
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
