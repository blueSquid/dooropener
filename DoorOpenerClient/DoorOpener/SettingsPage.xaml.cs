﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DoorOpener
{
    public partial class SettingsPage : ContentPage
    {
        private DoorModel model = null;

        public SettingsPage(ref DoorModel mModel )
        {
            InitializeComponent();
            this.model = mModel;
            this.BindingContext = this.model;
            this.model.PropertyChanged += Model_PropertyChanged;
            this.Disappearing += Handle_Disappearing;
        }


        private async void Handle_Disappearing(object sender, EventArgs e)
        {
            
            if (model.ConnectToSsidFlag == true)
            {
                if(this.model.Ssid == null || this.model.Ssid.Equals(""))
                {
                    await DisplayAlert("Unvalid SSID", "Will not attempt to connect to any SSID. Please enter a value into the wifi SSID field.", "Ok");
                }
            }


            //update the model from the view model
            FileWriter writer = new FileWriter();
            writer.SaveToFile(this.model);
            this.model.ConnectToSsidFlag.Equals(false);
        }

        private async void Handle_Unfocused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            try
            {
                float timeOutValue = model.TimeoutValueFloat;
                if(timeOutValue < 0)
                {
                    throw new Exception("Timeout value must be positive");
                }
            }
            catch(Exception exception)
            {
                await DisplayAlert("Unvalid timeout", "Please enter a value into the timeout field.", "Ok");
                bool isFocused = this.timeoutField.Focus();
                if (isFocused == false)
                {
                    throw new Exception("Unable to restore focus to the timeout field");
                }
            }
        }

        private void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "ConnectToSsidFlag")
            {
                if (this.model.ConnectToSsidFlag == true)
                {
                    this.model.EnableMobileNetworkFlag = false;
                }
            }
        }

    }
}
