﻿using System;
using Xamarin.Forms;

namespace DoorOpener
{
    public partial class MainPage : ContentPage
    {
        private DoorUtil doorUtil = null;
        private DoorModel model = null;

        public MainPage()
        {
            InitializeComponent();
            this.doorUtil = DependencyService.Get<DoorUtil>();

            FileWriter writer = new FileWriter();

            this.model = writer.RestoreFromFile();

            if(this.model == null) 
            {
                this.model = new DoorModel();
            }

            this.BindingContext = this.model;

            this.Appearing += Handle_Appearing;
        }

        private async void Handle_Appearing(object sender, EventArgs e)
        {
            try
            {
                if (this.model.ConnectToSsidFlag == true)
                {
                    //request network change
                    string ssid = this.model.Ssid;
                    if (ssid != null)
                    {
                        this.doorUtil.RequestNetworkChange(ssid);
                    }
                }
            }
            catch(Exception exception)
            {
                this.doorUtil.CancelPendingRequests();
                await DisplayAlert("Failed", "Failed to start app: " + exception.Message, "Ok");
            }
        }

        private async void Handle_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                //disable button while sending the request
                this.triggerBtn.IsEnabled = false;
                if(this.model.TriggerUrl == null) {
                    throw new Exception("please enter the trigger url in the settings.");
                }
                this.status.Text = "Working...";
                string urlAddress = this.model.TriggerUrl;
                bool enableModelNetwork = this.model.EnableMobileNetworkFlag;
                float timeoutVal = this.model.TimeoutValueFloat;
                await this.doorUtil.TriggerDoor(this.model.TriggerUrl, timeoutVal, this.model.EnableMobileNetworkFlag);
                this.status.Text = "Triggered";
            }
            catch(Exception exception)
            {
                this.doorUtil.CancelPendingRequests();
                await DisplayAlert("Failed", "Failed to open door: " + exception.Message, "Ok");
                this.status.Text = "Failed";
            }
            finally
            {
                this.triggerBtn.IsEnabled = true;
            }
        }

        private async void Handle_Tapped(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new SettingsPage(ref this.model));
        }
    }
}
