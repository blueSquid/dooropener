﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace DoorOpener
{
    public abstract class DoorUtil
    {
        WebClientExtended webClient = null;

        protected DoorUtil()
        {
            this.webClient = new WebClientExtended();

            //don't want the cache to get in the way of openning the garage door
            this.webClient.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.BypassCache);
        }

        public void RequestNetworkChange(string ssid)
        {
            this.SwitchNetworkIfRequired(ssid);
        }

        protected abstract void SwitchNetworkIfRequired(string targetSsid);

        public abstract string GetIpAddress();

        public async Task TriggerDoor(string url, float timeout, bool enableMobileNetwork)
        {
            int timeoutValue = (int)(timeout * 1000.0); //milliseconds
            Task sendHttpTask = this.SendHTTPGet(url, enableMobileNetwork);
            if( await Task.WhenAny(sendHttpTask, Task.Delay(timeoutValue)) == sendHttpTask)
            {
                //http timeout did not occur check for exceptions
                if(sendHttpTask?.Exception?.InnerExceptions?.Count > 0)
                {
                    //just print out the first inner exception
                    throw sendHttpTask.Exception.InnerExceptions[0];
                }
            }
            else
            {
                //timeout occured
                throw new TimeoutException("timout occured. Make sure you are on the right network");
            }
        }

        public void CancelPendingRequests()
        {
            webClient.CancelAsync();
        }

        private async Task SendHTTPGet(string url, bool enableMobileNetwork)
        {
            
            IPAddress curIpAddress = null;
            if(enableMobileNetwork == false)
            {
                curIpAddress = IPAddress.Parse(this.GetIpAddress());
            }
            string response = await webClient.DownloadStringTaskExtendAsync(url, curIpAddress);
            if (response == null)
            {
                throw new HttpRequestException("No Response");
            }
        }
    }
}
