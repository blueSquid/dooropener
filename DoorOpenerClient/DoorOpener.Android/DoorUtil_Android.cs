﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using DoorOpener.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(DoorUtil_Android))]
namespace DoorOpener.Droid
{
    public class DoorUtil_Android : DoorUtil
    {
        public DoorUtil_Android()
        {
        }

        protected override void SwitchNetworkIfRequired(string targetSsid)
        {
            string networkSSID = targetSsid;

            WifiManager wifiManager = (WifiManager)Android.App.Application.Context.GetSystemService(Context.WifiService);
            IList<WifiConfiguration> myWifi = wifiManager.ConfiguredNetworks;
            WifiConfiguration targetWifi = myWifi.FirstOrDefault(x => x.Ssid.Contains(networkSSID));

            if (targetWifi == null)
            {
                throw new ApplicationException("This phone has never log into " + networkSSID + " before.");
            }

            if(wifiManager.IsWifiEnabled == false) 
            {
                wifiManager.SetWifiEnabled(true);
            }

            string currentSsid = wifiManager.ConnectionInfo.SSID;

            //Oreo v8.1+ and above can have permission issues restricting the application from retrieving the current ssid 
            if(currentSsid == "<unknown ssid>") 
            {
                // can use the connectivity manager service to attempt to retrieve the ssid
                ConnectivityManager connectivityManager = (ConnectivityManager)
                    Android.App.Application.Context.GetSystemService(
                        Context.ConnectivityService);

                //found that extrainfo sometimes contains the ssid even if the app hasn't given permission to access it (silly android)
                NetworkInfo activeNetworkInfo = connectivityManager.ActiveNetworkInfo;
                currentSsid = activeNetworkInfo.ExtraInfo;
            }

            if (currentSsid.Contains(networkSSID))
            {
                //already connected
                return;
            }

            //connect to the new network
            wifiManager.EnableNetwork(targetWifi.NetworkId, true);
            wifiManager.Reconnect();

        }

        public override string GetIpAddress() {
            WifiManager wifiManager = (WifiManager)Android.App.Application.Context.GetSystemService(Context.WifiService);
            int ip = wifiManager.ConnectionInfo.IpAddress;

            if(ip == 0)
            {
                //default ip value
                throw new WebException("Failed retrieve local IP address. This is most likely because you are not connected to wifi.");
            }
            IPAddress ipAddress = new IPAddress(ip);
            return ipAddress.ToString();
        }
    }
}
