from http.server import BaseHTTPRequestHandler, HTTPServer
import atexit
from urllib.parse import urlparse
from DoorTrigger import DoorTrigger
import configparser

class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
 
    # GET
    def do_GET(self):
        # tell client that their request has been acknowledged
        self.send_response(200)
        
        # Send headers
        self.end_headers()
 
        parsed_path = urlparse(self.path)
        message = parsed_path.path + parsed_path.query
        if parsed_path.path == doorPath and parsed_path.query == doorParam:
            message = "triggered"

            doorTrigger.triggerDoor(pressTime)
        
        # Send message back to client
        # Write content as utf-8 data
        self.wfile.write(bytes(message, "utf8"))
        return
        

#entry point
print('starting server...')

config = configparser.ConfigParser()
config.read('settings.ini')
doorPath = config['SERVER']['urlPath']
doorParam = config['SERVER']['urlGetParam']
serverPortNumber = int(config['SERVER']['serverPortNum'])
doorGPIOPin = int(config['SERVER']['serverBcmGpioPinNum'])
pressTime = float(config['SERVER']['simulatedPressTime'])
serverIp = config['SERVER']['serverIpAddress']

# Server settings
server_address = (serverIp, serverPortNumber)
activeServer = HTTPServer(server_address, testHTTPServer_RequestHandler)
def close_socket():
    # called when script is interrupted
    print("closing server")
    #tell the OS that the listening port is no longer being used
    activeServer.socket.close()
atexit.register(close_socket)

doorTrigger = DoorTrigger(doorGPIOPin)

#launch server
print('running server...')
activeServer.serve_forever()