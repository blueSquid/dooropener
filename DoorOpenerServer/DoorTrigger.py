import RPi.GPIO as GPIO
import time
class DoorTrigger:
    _activePin = 0
    
    def __init__(self, pin):
        self._activePin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._activePin, GPIO.OUT)

    def triggerDoor(self, pressTime):
        GPIO.output(self._activePin, True)
        time.sleep(pressTime)
        GPIO.output(self._activePin, False)